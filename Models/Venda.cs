using System;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public string ItemVendido { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}