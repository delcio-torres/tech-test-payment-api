
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller])")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("Cadastrar Vendedor")]
        public IActionResult AddVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpPost("Cadastrar Venda")]
        public IActionResult AddVenda(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("Obter por {id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if(venda == null) return NotFound();

            return Ok(venda);
        }

        [HttpPut("Atualizar venda {id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();


            vendaBanco.ItemVendido = venda.ItemVendido;
            vendaBanco.IdVendedor = venda.IdVendedor;
            vendaBanco.Data = venda.Data;
            vendaBanco.Status = venda.Status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

    }
}